import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../guards/auth.guard';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromesasComponent } from './promesas/promesas.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { PerfilComponent } from './perfil/perfil.component';

// Mantenimientos
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';
import { HospitalesComponent } from './mantenimientos/hospitales/hospitales.component';
import { MedicosComponent } from './mantenimientos/medicos/medicos.component';
import { MedicoComponent } from './mantenimientos/medicos/medico.component';

// Pacientes
import { MisPacientesComponent } from './medicos/mis-pacientes/mis-pacientes.component';
import { ExamenesComponent } from './tecnologos/examenes/examenes.component';
import { FormularioPacientesComponent } from './secretaria/formulario-pacientes/formulario-pacientes.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { AdminGuard } from '../guards/admin.guard';
// Medicos




const routes: Routes = [
    {
        path: 'dashboard',
        component: PagesComponent,
        canActivate: [ AuthGuard ],
        children: [
            { path: '', component: DashboardComponent, data: { titulo: 'Dashboard' } },
            { path: 'progress', component: ProgressComponent, data: { titulo: 'ProgressBar' }},
            { path: 'buscar/:termino', component: BusquedaComponent, data: { titulo: 'Busquedas' }},
            { path: 'grafica1', component: Grafica1Component, data: { titulo: 'Gráfica #1' }},
            { path: 'account-settings', component: AccountSettingsComponent, data: { titulo: 'Ajustes de cuenta' }},
            { path: 'promesas', component: PromesasComponent, data: { titulo: 'Promesas' }},
            { path: 'rxjs', component: RxjsComponent, data: { titulo: 'RxJs' }},
            { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil de usuario' }},

            // Mantenimientos
           // { path: 'usuarios', component: UsuariosComponent, data: { titulo: 'Usuarios de aplicacion' }},
            { path: 'hospitales', component: HospitalesComponent, data: { titulo: 'Hospitales de aplicacion' }},
            { path: 'medicos', component: MedicosComponent, data: { titulo: 'Médicos de aplicacion' }},
            { path: 'medico/:id', component: MedicoComponent, data: { titulo: 'Médicos de aplicacion' }},

            // Médicos
            { path: 'mis-pacientes', component: MisPacientesComponent, data: { titulo: 'Mis pacientes' }},

            // Tecnólogos
            { path: 'examenes', component: ExamenesComponent, data: { titulo: 'Exámenes' }},

            // Tecnólogos


            // Secretaria
            { path: 'formulario-paciente', component: FormularioPacientesComponent, data: { titulo: 'Formulario pacientes' }},
            { path: 'lista-pacientes', component: FormularioPacientesComponent, data: { titulo: 'Listar pacientes' }},

             // Rutas de Admin
             { path: 'usuarios', canActivate: [ AdminGuard ], component: UsuariosComponent, data: { titulo: 'Matenimiento de Usuarios' }},
     
        ]
    },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class PagesRoutingModule {}


