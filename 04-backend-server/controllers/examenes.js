const Examen = require('../models/examen');

const getExamenes =  async (req,res )=> {

    const examenes = await Examen.find()
                        .populate('usuario','nombre email')
                        .populate('paciente','nombre apellido1 apellido2 rut telefono')

     res.json({
        ok: true, 
        examenes
     })

}

const getExamenById = async(req, res = response) => {

    const id = req.params.id;

    try {
        const examen = await Examen.findById(id)
            .populate('usuario', 'nombre img')
            .populate('paciente','nombre apellido1 apellido2 rut telefono');

        res.json({
            ok: true,
            examen
        })

    } catch (error) {
        console.log(error);
        res.json({
            ok: true,
            msg: 'Hable con el administrador'
        })
    }
}

const crearExamen = async(req,res )=> {

    const uid = req.uid;
    
    const examen = new Examen({
        usuario: uid,
        ...req.body,
    });

    try{

        const  examenDB =  await examen.save();

        res.json({
            ok:true,
            examenDB
        })

    } catch(error){
        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }

}
const actualizarExamen = async (req,res )=> {

    const id = req.params.id;
    const uid = req.uid;

    const cambioExamen = ({
        usuario: uid,
        ...req.body
    })

    try{

        const examenDB = await Examen.findById(id);

        if( !examenDB ){
            res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el ID del examen'
            })
        }

    examenActualizado = await Examen.findByIdAndUpdate(id, cambioExamen, { new: true });

    res.json({
        ok:true,
        examenDB: examenActualizado
    })

    } catch{
        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })

    }
}

const borrarExamen = async (req,res )=> {

    id = req.params.id;

    try{
        const examenDB = await Examen.findById(id);

        if(!examenDB){
            res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el ID del Examen'
            })
        }
    
        examenBorrado = await Examen.findByIdAndDelete(id)
    
        res.json({
            ok: true,
            msg: 'Examen eliminado correctamente'
        });

    } catch {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })


    }
   

}

module.exports = {
    getExamenes,
    getExamenById,
    crearExamen,
    actualizarExamen,
    borrarExamen
}