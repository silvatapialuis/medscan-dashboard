const Paciente = require('../models/paciente');

const getPacientes =  async (req,res )=> {

    const pacientes = await Paciente.find()
                        .populate('usuario','nombre email')

     res.json({
        ok: true, 
        pacientes
     })

}

const getPacienteById = async(req, res = response) => {

    const id = req.params.id;

    try {
        const paciente = await Paciente.findById(id)
            .populate('usuario', 'nombre img')

        res.json({
            ok: true,
            paciente
        })

    } catch (error) {
        console.log(error);
        res.json({
            ok: true,
            msg: 'Hable con el administrador'
        })
    }
}

const crearPaciente = async (req,res )=> {

    const uid = req.uid;
    
    const paciente = new Paciente({
        usuario: uid,
        ...req.body,
    });

    try{

        const  pacienteDB =  await paciente.save();

        res.json({
            ok:true,
            pacienteDB
        })

    } catch(error){
        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }

}
const actualizarPaciente = async (req,res )=> {

    const id = req.params.id;
    const uid = req.uid;

    const cambioPaciente = {
        usuario: uid,
        ...req.body
    }

    try{

        const pacienteDB = await Paciente.findById(id);

        if( !pacienteDB ){
            res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el ID del paciente'
            })
        }

    pacienteActualizado = await Paciente.findByIdAndUpdate(id, cambioPaciente, { new: true });

    res.json({
        ok:true,
        pacienteDB: pacienteActualizado
    })

    } catch{
        console.log(error)
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })

    }
}

const borrarPaciente = async (req,res )=> {

    id = req.params.id;

    try{
        const pacienteDB = await Paciente.findById(id);

        if(!pacienteDB){
            res.status(404).json({
                ok: false,
                msg: 'No se ha encontrado el ID del paciente'
            })
        }
    
        pacienteBorrado = await Paciente.findByIdAndDelete(id)
    
        res.json({
            ok: true,
            msg: 'Paciente eliminado correctamente'
        });

    } catch {

        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })


    }
   

}

module.exports = {
    getPacientes,
    crearPaciente,
    actualizarPaciente,
    borrarPaciente,
    getPacienteById
}