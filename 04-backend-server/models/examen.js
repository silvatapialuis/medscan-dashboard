const { Schema, model } = require('mongoose');

const ExamenSchema = Schema({

    tipoExamen:{
        type: String,
        required: true,
    },
    fechaIngreso: {
        type: Date,
        required:true,
    },
    formaPago: {
        type: String,
        required: true,
    },
    radioFarmaco: {
        type: String,
        required:false,
    },
    diagnostico: {
        type: String,
        required:false,
    },
    horaIngreso: {
        type: Date,
        required:false,
    },
    observaciones:{
        type: String,
        required: false,

    },
    usuario:{
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true,
    },
    paciente: {
        type: Schema.Types.ObjectId,
        ref: 'Paciente',
        required: true,
    },
    estado: {
        type: String,
        required: true,
    },
});

ExamenSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
})


module.exports = model('Examen', ExamenSchema);