const { Schema, model } = require('mongoose');

const PacienteSchema = Schema({
    nombre: {
        type: String,
        required: true,
    },
    apellido1: {
        type: String,
        required: true,
    },
    apellido2: {
        type: String,
        required: true,
    },
    rut: {
        type: String,
        required: true,
    },
    fechaNac: {
        type: Date,
        required: true,
    },
    edad: {
        type: Number,
        required: false,
    },
    peso: {
        type: Number,
        required:false,

    },
    talla: {
        type: Number,
        required: false,
    },
    telefono:{
        type: Number,
        required:true

    },
    correo: {
        type: String,
        required: false,
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true,
    },
 });

 

 PacienteSchema.method('toJSON', function() {
     const { __v,...object } = this.toObject();
     return object;
 })



module.exports = model( 'Paciente', PacienteSchema );
