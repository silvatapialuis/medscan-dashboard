// Ruta api/examenes

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { getExamenes,
        crearExamen,
        actualizarExamen,
        borrarExamen,
        getExamenById
    
} = require('../controllers/examenes');

const router = Router();

router.get('/',     
    validarJWT,
    getExamenes );

router.get('/:id',
    validarJWT,
    getExamenById
);

router.post('/',
    [
        validarJWT,
        check('tipoExamen').not().isEmpty()
            .withMessage('El tipo de examen no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El tipo de examen debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El tipo de examen solo debe contener letras'),
        check('fechaIngreso').not().isEmpty()
            .withMessage('La fecha no debe estar vacia'),
        check('formaPago').not().isEmpty()
            .withMessage('La forma de pago no debe estar vacia')
            .isLength({ min:3 })
            .withMessage('La forma de pago debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('La forma de pago solo debe contener letras'),
       //  check('horaIngreso').not().isEmpty()
       //     .withMessage('La fecha no debe estar vacia'),
        check('estado').not().isEmpty()
            .withMessage('El estado no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El estado debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El estado solo debe contener letras'),
        validarCampos
    ],
    crearExamen
);

router.put('/:id',
    [
        validarJWT,
        check('tipoExamen').not().isEmpty()
            .withMessage('El tipo de examen no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El tipo de examen debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El tipo de examen solo debe contener letras'),
        check('fechaIngreso').not().isEmpty()
            .withMessage('La fecha de ingreso no debe estar vacia'),
        check('formaPago').not().isEmpty()
            .withMessage('La forma de pago no debe estar vacia')
            .isLength({ min:3 })
            .withMessage('La forma de pago debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('La forma de pago solo debe contener letras'),
         //check('horaIngreso').not().isEmpty()
         //   .withMessage('La fecha no debe estar vacia'),
        check('estado').not().isEmpty()
            .withMessage('El estado no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El estado debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El estado solo debe contener letras'),
        validarCampos
    ] ,
    actualizarExamen
);

router.delete('/:id',
     validarJWT,
    borrarExamen
);

module.exports = router;
