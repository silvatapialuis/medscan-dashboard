// Ruta api/pacientes

const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');
const { validarJWT } = require('../middlewares/validar-jwt');

const { getPacientes,
        crearPaciente,
        actualizarPaciente,
        borrarPaciente,
        getPacienteById
} = require('../controllers/pacientes');

const router = Router();

router.get('/', 
    validarJWT,
    getPacientes );

router.get('/:id',
    validarJWT,
    getPacienteById
);

router.post('/',
    [
        validarJWT,
        check('nombre').not().isEmpty()
            .withMessage('El nombre no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El nombre debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El nombre solo debe contener letras'),
        check('apellido1').not().isEmpty()
            .withMessage('El apellido no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El apellido debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El apellido solo debe contener letras'),
        check('apellido2').not().isEmpty()
            .withMessage('El apellido no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El apellido debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El apellido solo debe contener letras'),
        check('rut').not().isEmpty()
            .withMessage('El rut no debe estar vacio')
            .isLength({ min:10 })
            .withMessage('El rut debe tener minimo 10 caracteres'),
         // TODO: Funcion que calcule el RUT
         check('fechaNac').not().isEmpty()
         .withMessage('La fecha no debe estar vacia'),
         check('telefono').not().isEmpty()
            .withMessage('El telefono no debe estar vacio')
            .isLength({ min:9 })
            .withMessage('El telefono debe tener minimo 9 caracteres')
            .matches(/\d/)
            .withMessage('El telefono solo debe contener numeros'),
        validarCampos 
    ],
    crearPaciente
);

router.put('/:id',
    [
        validarJWT,
        check('nombre').not().isEmpty()
            .withMessage('El nombre no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El nombre debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El nombre solo debe contener letras'),
        check('apellido1').not().isEmpty()
            .withMessage('El apellido no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El apellido debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El apellido solo debe contener letras'),
        check('apellido2').not().isEmpty()
            .withMessage('El apellido no debe estar vacio')
            .isLength({ min:3 })
            .withMessage('El apellido debe tener minimo 3 caracteres')
            .matches((/[A-Z][a-z]/))
            .withMessage('El apellido solo debe contener letras'),
        check('rut').not().isEmpty()
            .withMessage('El rut no debe estar vacio')
            .isLength({ min:10 })
            .withMessage('El rut debe tener minimo 10 caracteres'),
         /////////////////////////////////////////////////////////////////
          //   TODO: Funcion que calcule que el RUT tiene formato valido
         /////////////////////////////////////////////////////////////////
         check('fechaNac').not().isEmpty()
         .withMessage('La fecha no debe estar vacia'),
         check('telefono').not().isEmpty()
            .withMessage('El telefono no debe estar vacio')
            .isLength({ min:9 })
            .withMessage('El telefono debe tener minimo 9 caracteres')
            .matches(/\d/)
            .withMessage('El telefono solo debe contener numeros'),
        validarCampos
    ] ,
    actualizarPaciente
);

router.delete('/:id',
     validarJWT,
    borrarPaciente
);

module.exports = router;
